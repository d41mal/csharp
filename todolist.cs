using System;
using System.Collections.Generic;

namespace todolist
{
    class Program
    {
        public static void SaveFile(List<string> items, string fileName)
        {
            System.IO.File.WriteAllLines(string.Format(@"C:\Users\david\Desktop\notes\todolist\{0}.txt",fileName), items);

        }


        static void Main(string[] args)
        {
            string item;

            List<string> toDoList = new List<string>();
            Console.Write("Enter file name: ");
            var fileName = Console.ReadLine();
            Console.WriteLine("Enter Q to quit");
            do
            {
                item = Convert.ToString(Console.ReadLine());
                if (item != "Q")
                {
                    toDoList.Add(item);
                }

                else
                {
                    break;
                }

            }
            while (item != "Q");
            SaveFile(toDoList, fileName);

        }
    }
}